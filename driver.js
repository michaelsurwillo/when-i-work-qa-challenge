//Property of Michael Surwillo

'use strict'

const webdriver = require('selenium-webdriver');
const settings = webdriver.Capabilities.chrome();
const chrome = require('chromedriver').path;
settings.set("chrome.binary.path", chrome);
const driver = new webdriver.Builder().withCapabilities(settings).build();

//exports the driver
module.exports = driver;