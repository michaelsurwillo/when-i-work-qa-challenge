//Property of Michael Surwillo

'use strict';

const { By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
//driver from driver.js
const driver = require('./driver.js');

//web element variable
let element;

//test that verifies the correct red box is present on an incorrect password
//and verifies text in the red box is correct
//and verifies that "Schedule a Shift" and "Watch a Video" buttons are present on the dashboard when
//there is not a shift scheduled for today
describe('running qachallenge_test_1.js', function() {
    it('open https://login.wheniwork.com', async function() {
        await driver.get('https://login.wheniwork.com/');
        //maximize window
        await driver.manage().window().maximize();
    })
    it('enter incorrect email address and password', async function() {
        await driver.findElement(By.id('email')).sendKeys('surwillo.michael@uwlax.edu', Key.ENTER);
        await driver.findElement(By.id('password')).sendKeys('incorrectpassword', Key.ENTER);
    });
    it('verify password was incorrect', async function() {
        //this line throws an exception if red box does not show up when password is incorrect
        element = await driver.wait(until.elementLocated(By.xpath("//div[@class='notice alert alert-danger']"))).getText();
        //this line throws an assertion error if red box text does not match "Incorrect username and/or password. Please try again."
        assert.equal(element, "Incorrect username and/or password. Please try again.");
    });
    it('clear password field', async function() {
        await driver.findElement(By.id('password')).sendKeys(Key.CONTROL + "a");
        await driver.findElement(By.id('password')).sendKeys(Key.DELETE);
    });
    it('enter correct password', async function() {
        await driver.findElement(By.id('password')).sendKeys('qachallenge', Key.ENTER);
    });
    it('verify "Schedule a Shift" is present', async function() {
        //this line throws an exception if xpath element is not there
        element = await driver.wait(until.elementLocated(By.xpath("//ul[@class='list today-list']//a[1]"))).getText();
        //this line throws an assertion error if the element text does not match "Schedule a Shift"
        assert.equal(element, "Schedule a Shift");
    });
    it('verify "Watch a Video" is present', async function() {
        //this line throws an exception if xpath element is not there
        element = await driver.wait(until.elementLocated(By.xpath("//ul[@class='list today-list']//a[2]"))).getText();
        //this line throws an assertion error if the element text does not match "Watch a Video"
        assert.equal(element, "Watch a Video");
    });
    //exits Google Chrome
    after(() => driver && driver.quit());
});