//Property of Michael Surwillo

'use strict';

const { By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
//driver from driver.js
const driver = require('./driver.js');

//test that verifies "NO SHIFTS" and "ADD SHIFTS TO PUBLISH" are present
//and adds a shift for today for Michael Surwillo
//and verifies that "PUBLISH & NOTIFY" and "ALL POSITIONS" are present
//and verifies that the shift is published
describe('running qachallenge_test_2.js', function() {
    it('open https://login.wheniwork.com', async function() {
        await driver.get('https://login.wheniwork.com/');
        //maximize window
        await driver.manage().window().maximize();
    })
    it('enter correct email address and password', async function() {
        await driver.findElement(By.id('email')).sendKeys('surwillo.michael@uwlax.edu', Key.ENTER);
        await driver.findElement(By.id('password')).sendKeys('qachallenge', Key.ENTER);
    });
    it('verify scheduler button is present then click', async function() {
        //this line throws an exception if the Scheduler tab on the dashboard is not present
        await driver.wait(until.elementLocated(By.xpath("//span[.='Scheduler']")));
        await driver.findElement(By.xpath("//span[.='Scheduler']")).click();
    });
    it('verify "NO SHIFTS" is present', async function() {
        //this line throws an exception if "NO SHIFTS" is not present
        await driver.wait(until.elementLocated(By.xpath("//div[.='No Shifts']")));
    });
    it('verify "ADD SHIFTS TO PUBLISH" is present', async function() {
        //this line throws an exception if "ADD SHIFTS TO PUBLISH" is not present
        await driver.wait(until.elementLocated(By.xpath("//div[.='Add Shifts To Publish']")));
    });
    it('add shift for today for Michael Surwillo', async function() {
        //this line throws an exception if xpath element is not present
        await driver.wait(until.elementLocated(By.xpath("//div[@class='col schedule-cell show-plus today' and contains(@id, 'uid-34920186')]")));
        await driver.findElement(By.xpath("//div[@class='col schedule-cell show-plus today' and contains(@id, 'uid-34920186')]")).click();
        await driver.findElement(By.xpath("//span[.='CEO']")).click();
    });
    it('verify "PUBLISH & NOTIFY" is present', async function() {
        //this line throws an exception if "PUBLISH & NOTIFY" is not present
        await driver.wait(until.elementLocated(By.xpath("//div[.='Publish & Notify']")));
    });
    it('verify "ALL POSITIONS" is present', async function() {
        //this line throws an exception if "ALL POSITIONS" is not present
        await driver.wait(until.elementLocated(By.xpath("//div[.='All Positions']")));
    });
    it('publish and notify', async function() {
        await driver.findElement(By.xpath("//div[.='Publish & Notify']")).click();
    });
    it('verify shifts have been published notification is present', async function() {
        //this line throws an exception if the green box with text "Schedule notifications have been successfully sent" is not present
        await driver.wait(until.elementLocated(By.xpath("//span[.='Schedule notifications have been successfully sent.']")));
    });
    //exits Google Chrome
    after(() => driver && driver.quit());
});