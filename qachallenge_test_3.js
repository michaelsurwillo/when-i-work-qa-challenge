//Property of Michael Surwillo

'use strict';

const { By, Key, until } = require('selenium-webdriver');
const assert = require('assert');
//driver from driver.js
const driver = require('./driver.js');

//web element variable
let element;

//test that verifies "EVERYTHING PUBLISHED NO CHANGES" is present
//and clicks on drop down menu for Michael Surwillo
//and verifies button reads "Delete Michael's Shifts"
//and deletes shift
//and verifies shift was deleted
describe('running qachallenge_test_3.js', function() {
    it('open https://login.wheniwork.com', async function() {
        await driver.get('https://login.wheniwork.com/');
        //maximize window
        await driver.manage().window().maximize();
    })
    it('enter correct email address and password', async function() {
        await driver.findElement(By.id('email')).sendKeys('surwillo.michael@uwlax.edu', Key.ENTER);
        await driver.findElement(By.id('password')).sendKeys('qachallenge', Key.ENTER);
    });
    it('verify scheduler button is present then click', async function() {
        //this line throws an exception if the Scheduler tab on the dashboard is not present
        await driver.wait(until.elementLocated(By.xpath("//span[.='Scheduler']")));
        await driver.findElement(By.xpath("//span[.='Scheduler']")).click();
    });
    it('verify "EVERYTHING PUBLISHED NO CHANGES" is present', async function() {
        element = await driver.wait(until.elementLocated(By.xpath("//div[@class='schedule-sidebar']//div[@role='group']//button"))).getText();
        //this line throws an assertion error if button from given xpath does not match "EVERYTHING PUBLISHED NO CHANGES"
        assert.equal(element, "EVERYTHING PUBLISHED\nNO CHANGES");
    });
    it('delete the added shift for Michael Surwillo', async function() {
        //this line throws an exception if the drop down menu cannot be located
        await driver.wait(until.elementLocated(By.xpath("//a[@id='user-menu-34920186']")));
        await driver.findElement(By.xpath("//a[@id='user-menu-34920186']")).click();
        element = await driver.wait(until.elementLocated(By.xpath("//button[@class='dropdown-item delete-shifts']"))).getText();
        //this line throws an assertion error if the xpath button text does not match "Delete Michael's Shifts"
        assert.equal(element, "Delete Michael's Shifts");
        await driver.findElement(By.xpath("//button[@class='dropdown-item delete-shifts']")).click();
        //this line throws an exception if the 'Okay' button cannot be located
        await driver.wait(until.elementLocated(By.xpath("//div[.='Okay']")));
        await driver.findElement(By.xpath("//div[.='Okay']")).click();
    });
    it('verify shift was deleted', async function() {
        await driver.wait(until.elementLocated(By.xpath("//div[.='No Shifts']")));
        element = await driver.wait(until.elementLocated(By.xpath("//div[@class='schedule-sidebar']//div[@role='group']//button"))).getText();
        //this line throws an assertion error if the xpath button text does not match "NO SHIFTS ADD SHIFTS TO PUBLISH"
        assert.equal(element, "NO SHIFTS\nADD SHIFTS TO PUBLISH");
    });
    //exits Google Chrome
    after(() => driver && driver.quit());
});