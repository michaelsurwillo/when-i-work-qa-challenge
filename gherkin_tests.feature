Feature : qa challenge Scenarios

  # Scenario ID: qachallenge_test_1
  # Author: Michael Surwillo

  Scenario: Incorrectly login to https://login.wheniwork.com/, then correcly login to https://login.wheniwork.com/
    Given I am at https://login.wheniwork.com/
    When I enter "surwillo.michael@uwlax.edu" as the email
    And I enter "incorrectpassword" as the password
    Then I should see "Incorrect usernmae and/or password. Please try again"
    And I should still https://login.wheniwork.com/ as the url
    When I enter "surwillo.michael@uwlax.edu" as the email
    And I enter "qachallenge" as the password
    Then I should login to the dashboard
    Then I should see a button that reads "Schedule a Shift"
    And I should see a button that reads "Watch a Video"
  
# --------------------------------------------------------------------

  # Scenario ID: qachallenge_test_2
  # Author: Michael Surwillo

  Scenario: Add a shift for today for Michael Surwillo
    Given I am at https://login.wheniwork.com/
    When I enter "surwillo.michael@uwlax.edu" as the email
    And I enter "qachallenge" as the password
    Then I should login to the dashboard
    When I click on the Scheduler 
    Then I should see "NO SHIFTS"
    And I should see "ADD SHIFTS TO PUBLISH"
    When I click on today's day for Michael Surwillo
    Then I add the shift
    Then I should see "PUBLISH & NOTIFY" 
    And I should see "ALL POSITIONS"
    Then I click on "PUBLISH & NOTIFY"
    And verify that the green pop up box displaying "Schedule notifications have been successfully sent"
  
# --------------------------------------------------------------------
 
  # Scenario ID: qachallenge_test_3
  # Author: Michael Surwillo  

  Scenario: Delete the shift added for today for Michael Surwillo
    Given I am at https://login.wheniwork.com/
    When I enter "surwillo.michael@uwlax.edu" as the email
    And I enter "qachallenge" as the password
    Then I should login to the dashboard
    When I click on the Scheduler 
    Then I should see the "EVERYTHING PUBLISHED NO CHANGES" button
    Then I click on the dropdown menu for Michael Surwillo
    And should see a button labeled "Delete Michael's Shifts"
    Then I click the "Okay" button
    Then I verify the shift was deleted when I see the "NO SHIFTS ADD SHIFTS TO PUBLISH" button