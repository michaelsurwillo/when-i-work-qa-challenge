#Michael Surwillo When I Work qa challenge code (information about tests is located at the bottom of page)

##Setting up your enviornment to run the tests
    1. install Node.js (which comes with npm)
    2. create a new root folder
        1. for example "when_i_work_challenge"
    3. using git bash (or mac equivalent), run the following commands
        1. "npm init"
            1. click enter through all prompts
        2. "npm install selenium-webdriver"
        3. "npm install chromedriver"
        4. "npm install mocha"
        4. "npm install chai"
    4. run the following command to clone my repository
        1. "git clone https://gitlab.com/michaelsurwillo/when-i-work-qa-challenge.git"
    5. run the following command to get to the correct folder
        1. "cd when-i-work-qa-challenge"
    6. you are now able to run the tests

##Running tests
    **note! you MUST run the tests in order (qachallenge_test_1 -> qachallenge_test_2 -> qachallenge_test_3)**
    **qachallenge_test_3 will fail if qa_challenge_test_2 is not run prior**
    1. There are three tests included in my repository, they are as follows
        1. qachallenge_test_1.js
        2. qachallenge_test_2.js
        3. qachallenge_test_3.js
    2. The command to run a test is "npm run <insert name of test>" with the name of tests being
        1. "qachallenge_test_1"
        2. "qachallenge_test_2"
        3. "qachallenge_test_3"
    3. For example
        1. "npm run qachallenge_test_1"
        2. "npm run qachallenge_test_2"
        3. "npm run qachallenge_test_3"    

##About the tests
    **qachallenge_test_1"
        1. This test uses mocha asserts to verify the text of certain web elements and compare that text to a given string to verify certain aspects of the UI are present
        2. More detail about the test can be found in comments in qachallenge_test_1.js
    **qachallenge_test_2"
        1. This test waits until elements are visible to verify that certain aspects of the UI are present
        2. More detail about the test can be found in comments in qachallenge_test_2.js
    **qachallenge_test_3"
        1. This test combines mocha asserts and waits until elements are visible to verify certain aspects of the UI are present
        2. More detail about the test can be found in comments in qachallenge_test_3.js
    **The Gherkin Test Files can be found in gherkin_tests.feature**
    **Along with comments, there are descriptions in each 'it' that describe what actions are happening**